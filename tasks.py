from celery import Celery

app = Celery(
    'tasks',
    backend='redis://10.189.37.243:6379/0',
    broker='redis://10.189.37.243:6379/1'
)


@app.task
def process_characters(task_options):
    print(task_options)


@app.task
def process_locations(task_options):
    print(task_options)


@app.task
def process_profanity(task_options):
    print(task_options)


@app.task
def process_sentiment(task_options):
    print(task_options)


@app.task
def process_word_count(task_options):
    print(task_options)
