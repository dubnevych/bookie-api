from typing import Union

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel

from celery import group
from tasks import process_characters, process_locations, process_sentiment, process_profanity, process_word_count
from redis import client

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


class AnalysisTask(BaseModel):
    book_path: Union[str, None]
    script_path: Union[str, None]
    word_count: bool
    characters: bool
    profanity: bool
    locations: bool
    sentiment: bool


@app.get("/")
def hello():
    return {'Hello': 'world'}


@app.get("/api/redis_test")
def redis_test():
    return client.Redis(host='10.189.37.243').get('foo')


@app.post("/api/analyze")
def analyze(task: AnalysisTask):
    name = None
    if task.book_path is not None:
        name = task.book_path.split('.')[0]
        name = name.replace('-', ' ')
    if task.script_path is not None:
        name = task.script_path.split('.')[0]
        name = name.replace('-', ' ')

    group([
        process_characters.s(task.dict()),
        process_locations.s(task.dict()),
        process_profanity.s(task.dict()),
        process_sentiment.s(task.dict()),
        process_word_count.s(task.dict()),
    ])()

    return {'report_path': f'results/{name}'}
